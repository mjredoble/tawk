###A Core Data application using Github Users API and using Codable NSManagedObject to parse JSON data and use it as an object for Core Data stuff.


Following issues to address: 

~~1. App does not automatically reload data once the internet is back. [GitHub users #3]. (FIXED)~~

~~2. No previously saved users are loaded from CD if the app starts with no internet [GitHub users #4]. (FIXED)~~

~~3. Search doesn't include note content. [Users list #7]. (FIXED)~~

4. No tests [Generic Requirements #8]. (CANNOT ACTIVATE TEST SINCE I USE NSManagedObject, Codable together - need research)

5. Network requests aren't queued to single request at a time [Generic Requirements #5]

~~6. CD writing should not be done on the main thread (note saving). (FIXED)~~
