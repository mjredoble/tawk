//
//  UIImage+Extension.swift
//  Tawk_Exam
//
//  Created by Michael Joseph Redoble on 07/06/2021.
//
// From: https://stackoverflow.com/questions/6672517/is-programmatically-inverting-the-colors-of-an-image-possible

import Foundation
import UIKit

extension UIImage {
    func inverseImage() -> UIImage? {
        if let filter = CIFilter(name: "CIColorInvert") {
            filter.setValue(CIImage(image: self), forKey: kCIInputImageKey)
            if let outputImage = filter.outputImage {
                let newImage = UIImage(ciImage: outputImage)
                return newImage
            }
        }
        
        return self
    }
}
