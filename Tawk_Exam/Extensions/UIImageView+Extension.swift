//
//  UIImage+Extension.swift
//  Tawk_Exam
//
//  Created by Michael Joseph Redoble on 06/06/2021.
//
//From: hackingwithswift.com/

import Foundation
import UIKit

extension UIImageView {
    func loadAndsave(url: URL, filename: String) {
        DispatchQueue.global().async { [weak self] in
            if let data = try? Data(contentsOf: url) {
                if let image = UIImage(data: data) {
                    
                    let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)

                    //Save to documents directory
                    let filename = paths[0].appendingPathComponent(filename)
                    try? data.write(to: filename)
                    
                    DispatchQueue.main.async {
                        self?.image = image
                    }
                }
            }
        }
    }
}
