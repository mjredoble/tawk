//
//  CodingUserInfoKey+Extension.swift
//  Tawk_Exam
//
//  Created by Michael Joseph Redoble on 05/06/2021.
//
// Base from the following sites it can set NSManagedObject as Decodable/Codable
// https://medium.com/swlh/core-data-using-codable-68660dfb5ce8
// https://www.donnywals.com/using-codable-with-core-data-and-nsmanagedobject/
// 

import Foundation

extension CodingUserInfoKey {
    static let managedObjectContext = CodingUserInfoKey(rawValue: "managedObjectContext")!
}
