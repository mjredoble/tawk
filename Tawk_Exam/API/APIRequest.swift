//
//  APIManager.swift
//  Tawk_Exam
//
//  Created by Michael Joseph Redoble on 05/06/2021.
//

import Foundation

final class APIRequest {
    var baseURL = "https://api.github.com/"
    var url = ""
    var httpMethod: String?
    var postData: NSMutableDictionary?
    var responseData: Data?
    var timeout = 0
    var responseCode = 0
    var networkRetryCount = 0
    
    
    func execute(completionHandler: @escaping (Data?) -> ()) {
        let url = URL(string: baseURL + url)!

        var request = URLRequest(url: url)
        request.timeoutInterval = TimeInterval(timeout)
        request.httpMethod = httpMethod
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")

        if let postData = postData,
           let httpBody = try? JSONSerialization.data(withJSONObject: postData, options: [.prettyPrinted]) {
            request.httpBody = httpBody
        }
        
        let task = URLSession.shared.dataTask(with: url) { data, response, error in
            if let error = error {
                print("HTTP Request Failed \(error)")
            }
            
            if let httpResponse = response as? HTTPURLResponse {
                self.responseCode = httpResponse.statusCode
                print("statusCode: \(httpResponse.statusCode)")
            }
            
            if let data = data {
                completionHandler(data)
            }
        }
        
        task.resume()
    }
}
