//
//  UserService.swift
//  Tawk_Exam
//
//  Created by Michael Joseph Redoble on 05/06/2021.
//

import Foundation
import UIKit
import CoreData

final class UserService {
    
    static var isLoading = false
    
    static func getUsers(startID: Int, context: NSManagedObjectContext, completionHandler: @escaping ([User]) -> ()) {
        
        if isLoading {
            return
        }
        
        isLoading = true
        
        let request = APIRequest()
        request.url = "users?since=\(startID)"
        request.httpMethod = "GET"
        
        request.execute(completionHandler: { result in
            
            isLoading = false
            
            if let data = result {
                
                let decoder = JSONDecoder()
                decoder.userInfo[CodingUserInfoKey.managedObjectContext] = context
                
                if let users = try? decoder.decode([User].self, from: data) {
                    completionHandler(users)
                }
            }
        })
    }
    
    static func getUserDetail(loginName: String,context: NSManagedObjectContext, completionHandler: @escaping (User) -> ()) {
        
        if isLoading {
            return
        }
        
        isLoading = true
        
        let request = APIRequest()
        request.url = "users/\(loginName)"
        request.httpMethod = "GET"
        
        request.execute(completionHandler: { result in
            
            isLoading = false
            
            if let data = result {
                let decoder = JSONDecoder()
                decoder.userInfo[CodingUserInfoKey.managedObjectContext] = context

                if let user = try? decoder.decode(User.self, from: data) {
                    completionHandler(user)
                }
            }
        })
    }
}
