//
//  User+CoreDataClass.swift
//  Tawk_Exam
//
//  Created by Michael Joseph Redoble on 05/06/2021.
//
// Base from the following sites it can set NSManagedObject as Decodable/Codable
// https://medium.com/swlh/core-data-using-codable-68660dfb5ce8
// https://www.donnywals.com/using-codable-with-core-data-and-nsmanagedobject/
//

import Foundation
import CoreData

enum DecoderConfigurationError: Error {
  case missingManagedObjectContext
}

@objc(User)
public class User: NSManagedObject, Codable {
    
    enum CodingKeys: CodingKey {
        case login, id, node_id, avatar_url, gravatar_id, url, html_url, followers_url, following_url, gists_url, starred_url, subscriptions_url, organizations_url, repos_url, events_url, received_events_url, type, site_admin, name, company, blog, location, email, hireable, bio, twitter_username, public_repos, public_gists, followers, following, created_at, updated_at
    }
    
    required convenience public init(from decoder: Decoder) throws {
        guard let context = decoder.userInfo[CodingUserInfoKey.managedObjectContext] as? NSManagedObjectContext else {
              throw DecoderConfigurationError.missingManagedObjectContext
        }
        
        self.init(context: context)

        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        //Use for user list API
        self.login = try container.decodeIfPresent(String.self, forKey: .login)
        self.id = try container.decodeIfPresent(Int64.self, forKey: .id) ?? 0
        self.avatar_url = try container.decodeIfPresent(String.self, forKey: .avatar_url)
        self.gravatar_id = try container.decodeIfPresent(String.self, forKey: .gravatar_id)
        self.html_url = try container.decodeIfPresent(String.self, forKey: .html_url)
        self.followers_url = try container.decodeIfPresent(String.self, forKey: .followers_url)
        self.following_url = try container.decodeIfPresent(String.self, forKey: .following_url)
        self.gists_url = try container.decodeIfPresent(String.self, forKey: .gists_url)
        self.starred_url = try container.decodeIfPresent(String.self, forKey: .starred_url)
        self.subscriptions_url = try container.decodeIfPresent(String.self, forKey: .subscriptions_url)
        self.organizations_url = try container.decodeIfPresent(String.self, forKey: .organizations_url)
        self.repos_url = try container.decodeIfPresent(String.self, forKey: .repos_url)
        self.events_url = try container.decodeIfPresent(String.self, forKey: .events_url)
        self.received_events_url = try container.decodeIfPresent(String.self, forKey: .received_events_url)
        self.type = try container.decodeIfPresent(String.self, forKey: .type)
        self.site_admin = try container.decodeIfPresent(Bool.self, forKey: .site_admin) ?? false
        
        //Use for user detail API
        self.name = try container.decodeIfPresent(String.self, forKey: .name)
        self.company = try container.decodeIfPresent(String.self, forKey: .company)
        self.blog = try container.decodeIfPresent(String.self, forKey: .blog)
        self.location = try container.decodeIfPresent(String.self, forKey: .location)
        self.email = try container.decodeIfPresent(String.self, forKey: .email)
        self.hireable = try container.decodeIfPresent(Bool.self, forKey: .hireable) ?? false
        self.bio = try container.decodeIfPresent(String.self, forKey: .bio)
        self.twitter_username = try container.decodeIfPresent(String.self, forKey: .twitter_username)
        self.public_repos = try container.decodeIfPresent(Int64.self, forKey: .public_repos) ?? 0
        self.public_gists = try container.decodeIfPresent(Int64.self, forKey: .public_gists) ?? 0
        self.followers = try container.decodeIfPresent(Int64.self, forKey: .followers) ?? 0
        self.following = try container.decodeIfPresent(Int64.self, forKey: .following) ?? 0
        self.created_at = try container.decodeIfPresent(String.self, forKey: .created_at)
        self.updated_at = try container.decodeIfPresent(String.self, forKey: .updated_at)
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        
        try container.encode(login, forKey: .login)
        try container.encode(id, forKey: .id)
        try container.encode(avatar_url, forKey: .avatar_url)
        try container.encode(gravatar_id, forKey: .gravatar_id)
        try container.encode(html_url, forKey: .html_url)
        try container.encode(followers_url, forKey: .followers_url)
        try container.encode(following_url, forKey: .following_url)
        try container.encode(gists_url, forKey: .gists_url)
        try container.encode(starred_url, forKey: .starred_url)
        try container.encode(subscriptions_url, forKey: .subscriptions_url)
        try container.encode(repos_url, forKey: .repos_url)
        try container.encode(events_url, forKey: .events_url)
        try container.encode(received_events_url, forKey: .received_events_url)
        try container.encode(type, forKey: .type)
        try container.encode(site_admin, forKey: .site_admin)
        try container.encode(organizations_url, forKey: .organizations_url)
    }
    
    func hasNotes() -> Bool {
        if let notes = self.notes {
            if notes.count > 0 {
                return true
            }
        }
        
        return false
    }
}
