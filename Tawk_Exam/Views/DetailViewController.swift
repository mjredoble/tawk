//
//  DetailViewController.swift
//  Tawk_Exam
//
//  Created by Michael Joseph Redoble on 05/06/2021.
//

import UIKit
import CoreData

class DetailViewController: NetworkAwareViewController {

    private let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext

    var user: User?
    var viewDataModel = [String]()
    var isConnectedToInternet = false
    
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var followersLabel: UILabel!
    @IBOutlet weak var followingLabel: UILabel!
    @IBOutlet weak var notesTextView: UITextView!
    @IBOutlet weak var tableViewBottomConstraints: NSLayoutConstraint!
    @IBOutlet weak var noInternetView: UIView!
    @IBOutlet weak var noInternetViewTopConstraints: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        monitorNetwork()
        
        self.title = user?.login?.uppercased()
        self.notesTextView.delegate = self
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(tap)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_ :)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_ :)), name: UIResponder.keyboardWillHideNotification, object: nil)

        if isConnectedToInternet {
            fetchUserDetail()
        }
        else {
            self.updateViewDataModel()
            fetchUserFromDB()
        }
    }
    
    //MARK: - Connectivity Methods
    override func hasInternetConnectivity() {
        isConnectedToInternet = true
        
        DispatchQueue.main.async {
            self.noInternetView.isHidden = true
            self.noInternetViewTopConstraints.constant = -34
        }
    }
    
    override func dontHaveInternetConnectivity() {
        isConnectedToInternet = false
        
        DispatchQueue.main.async {
            self.noInternetViewTopConstraints.constant = 0
            self.noInternetView.isHidden = false
        }
    }
    
    //MARK: - Save method
    func showSaveButton() {
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Save", style: .done, target: self, action: #selector(saveChanges))
    }
    
    func hideSaveButton() {
        self.navigationItem.rightBarButtonItem = nil;
    }
    
    @objc func saveChanges() {
        if notesTextView.text.count > 0 {
            user?.notes = self.notesTextView.text
            
            //Saving notes on background thread
            let privateContext = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
            privateContext.persistentStoreCoordinator = self.context.persistentStoreCoordinator
            privateContext.perform { [self] in
                // Code in here is now running "in the background" and can safely
                // do anything in privateContext.
                // This is where you will create your entities and save them.
            
                try? self.context.save()
                
                DispatchQueue.main.async {
                    self.navigationController?.popViewController(animated: true)
                }
            }
        }
        
        hideSaveButton()
        dismissKeyboard()
    }
    
    //MARK: - Keyboard Delegate Methods
    @objc func dismissKeyboard() {
        view.endEditing(true)
        self.tableViewBottomConstraints.constant = 0
    }

    @objc func keyboardWillShow(_ notification:Notification) {
        self.tableViewBottomConstraints.constant = -250
        scrollToBottom()
    }
    
    @objc func keyboardWillHide(_ notification:Notification) {
        self.tableViewBottomConstraints.constant = 0
    }
    
    func fetchUserFromDB() {
        do {
            if let id = user?.id {
                let request = User.fetchRequest() as NSFetchRequest<User>
                let pred = NSPredicate(format: "id == %@", "\(id)")
                request.predicate = pred
                
                let result = try self.context.fetch(request)
                if result.count > 0 {
                    self.updateViewDataModel()
                    
                    for item in result {
                        DispatchQueue.main.async {
                            self.notesTextView.text = item.notes
                        }
                    }
                }
                else {
                    self.fetchUserDetail()
                }
            }
        }
        catch let error {
            print("ERROR: \(error)")
        }
    }
    
    func fetchUserDetail() {
        if let login = user?.login {
            UserService.getUserDetail(loginName: login, context: context, completionHandler: { result in
                self.user = result
                self.fetchUserFromDB()
            })
        }
    }
    
    func updateViewDataModel() {
        if let user = self.user {
            self.viewDataModel.removeAll()
            self.viewDataModel.append(user.name ?? "") //0
            self.viewDataModel.append(user.company ?? "") //1
            self.viewDataModel.append(user.blog ?? "") //2
            self.viewDataModel.append(user.location ?? "") //3
            self.viewDataModel.append(user.email ?? "") //4
            self.viewDataModel.append(user.twitter_username ?? "") //5
            
            if let imageUrl = user.avatar_url {
                DispatchQueue.main.async {
                    self.followersLabel.text = "Followers: ".uppercased() + "\(user.followers) "
                    self.followingLabel.text = "Following: ".uppercased() + " \(user.following) "
                    
                    if self.isConnectedToInternet {
                        self.avatarImageView.loadAndsave(url: URL(string: imageUrl)!, filename: "\(user.id).png")
                    }
                    else {
                        //Load image from document directory
                        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
                        let imagesDirectory = paths[0].appendingPathComponent("\(user.id).png")
                        let image = UIImage(contentsOfFile: imagesDirectory.path)
                        self.avatarImageView.image = image
                    }
                    
                    self.tableView.reloadData()
                }
            }
        }
    }
}

extension DetailViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0:
            return 44
        case 1:
            return 0
        default:
            return 44
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 0:
            return "Information"
        case 1:
            return "Notes"
        default:
            return ""
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return viewDataModel.count
        case 1:
            return 1
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "DetailCell", for: indexPath)
            
            switch indexPath.row {
            case 0:
                cell.textLabel?.text = "NAME: \t\(viewDataModel[0])"
            case 1:
                cell.textLabel?.text = "COMPANY: \t\(viewDataModel[1])"
            case 2:
                cell.textLabel?.text = "BLOG: \t\(viewDataModel[2])"
            case 3:
                cell.textLabel?.text = "LOCATION: \t\(viewDataModel[3])"
            case 4:
                cell.textLabel?.text = "EMAIL: \t\(viewDataModel[4])"
                
            case 5:
                cell.textLabel?.text = "TWITTER: \t\(viewDataModel[5])"
            default:
                cell.textLabel?.text = ""
            }
            
            return cell
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "DetailCell", for: indexPath)
            return cell
        }
    }
    
    func scrollToBottom()  {
        DispatchQueue.main.async {
            let point = CGPoint(x: 0, y: self.tableView.contentSize.height + self.tableView.contentInset.bottom - self.tableView.frame.height)
            if point.y >= 0 {
                self.tableView.setContentOffset(point, animated: false)
            }
        }
    }
}

extension DetailViewController: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        DispatchQueue.main.async {
            self.showSaveButton()
        }
    }
}
