//
//  ViewController.swift
//  Tawk_Exam
//
//  Created by Michael Joseph Redoble on 05/06/2021.
//

import UIKit
import CoreData

final class MainViewController: NetworkAwareViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    @IBOutlet weak var noInternetView: UIView!
    @IBOutlet weak var noInternetViewTopConstraints: NSLayoutConstraint!
    
    private let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    private let searchController = UISearchController(searchResultsController: nil)
    private let loader = ImageLoader()
    
    private var fetchUserStartId = 0
    private var users = [User]()
    private var filteredUsers: [User] = []

    
    private var isSearchBarEmpty: Bool {
      return searchController.searchBar.text?.isEmpty ?? true
    }
    
    var isFiltering: Bool {
      return searchController.isActive && !isSearchBarEmpty
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let container = (UIApplication.shared.delegate as! AppDelegate).persistentContainer
        container.loadPersistentStores { storeDescription, error in
            container.viewContext.mergePolicy = NSMergeByPropertyObjectTrumpMergePolicy

            if let error = error {
                print("Unresolved error \(error)")
            }
        }
        
        tableView.backgroundColor = UIColor.clear
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Search Users"
        navigationItem.searchController = searchController
        definesPresentationContext = true
        
        
        self.fetchUserStartId = getLastFetchedID()
        
        monitorNetwork()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if hasInternetConnection {
            fetchUsers()
        }
        else {
            fetchUsersFromDB()
        }
    }
    
    //MARK: - Connectivity Methods
    override func hasInternetConnectivity() {
        DispatchQueue.main.async {
            self.noInternetView.isHidden = true
            self.noInternetViewTopConstraints.constant = -34
            self.fetchUsers()
        }
    }
    
    override func dontHaveInternetConnectivity() {
        DispatchQueue.main.async {
            self.noInternetViewTopConstraints.constant = 0
            self.noInternetView.isHidden = false
        }
    }
    
    //MARK: - Save/Retrieve last fetched ID
    func saveLastFetchedID(lastID: Int) {
        let defaults = UserDefaults.standard
        defaults.set(lastID, forKey: "LastID")
    }

    func getLastFetchedID() -> Int {
        let defaults = UserDefaults.standard
        if let id = defaults.object(forKey: "LastID") {
            return id as! Int
        }
        
        return 0
    }
    
    //MARK: - Hide/Show Activitry Indicator
    func showSpinner() {
        DispatchQueue.main.async {
            self.spinner.isHidden = false
            self.spinner.startAnimating()
        }
    }
    
    func stopSpinner() {
        DispatchQueue.main.async {
            self.spinner.isHidden = true
            self.spinner.stopAnimating()
        }
    }
    
    //MARK: - Fetch Users from DB
    func fetchUsersFromDB() {
        showSpinner()
        
        do {
            //Fetch data from Core Data
            let sort = NSSortDescriptor(key: "id", ascending: true)
            let request = User.fetchRequest() as NSFetchRequest<User>
            request.sortDescriptors = [sort]
            
            self.users = try self.context.fetch(request)

            DispatchQueue.main.async {
                self.tableView.reloadData()
                self.stopSpinner()
            }
        }
        catch let error {
            print("ERROR: \(error)")
        }
    }
    
    //MARK: - Fetch Users from API
    func fetchUsers() {
        showSpinner()
        
        UserService.getUsers(startID: fetchUserStartId, context: context, completionHandler: { result in
            do {
                self.users.append(contentsOf: result)
                try self.context.save()
                self.fetchUsersFromDB()
                
                if let last = self.users.last {
                    self.fetchUserStartId = Int(last.id)
                    self.saveLastFetchedID(lastID: Int(last.id))
                    print("LAST FETCHED ID:\(self.fetchUserStartId)")
                }
            }
            catch let error {
                print("ERROR: \(error)")
            }
        })
    }
}

//MAARK: - UITableview Delegate Methods
extension MainViewController: UITableViewDelegate, UITableViewDataSource {
    private func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return .leastNormalMagnitude
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isFiltering {
            return filteredUsers.count
        }
        
        return users.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let user: User
        
        if isFiltering {
            user = filteredUsers[indexPath.row]
        }
        else {
            user = users[indexPath.row]
        }

        if user.hasNotes() {
           let cell = tableView.dequeueReusableCell(withIdentifier: "NotesCell", for: indexPath) as! UserWithNoteTableViewCell
            cell.selectionStyle = UITableViewCell.SelectionStyle.default
            cell.userNameLabel.text = user.login
            cell.userDetailLabel.text = user.html_url
            loadImageFor(cell: cell, url: user.avatar_url ?? "", indexPath: indexPath)
            
            return cell
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "UserCell", for: indexPath) as! UserTableViewCell
            cell.selectionStyle = UITableViewCell.SelectionStyle.default
            cell.userNameLabel.text = user.login
            cell.userDetailLabel.text = user.html_url
            loadImageFor(cell: cell, url: user.avatar_url ?? "", indexPath: indexPath)
            
            return cell
        }
    }
    
    func loadImageFor(cell: UserTableViewCell, url: String, indexPath: IndexPath) {
        let imageUrl = URL(string: url)
        let token = loader.loadImage(imageUrl!) { result in
          do {
            let image = try result.get()

            DispatchQueue.main.async {
              cell.userAvatarImageView.image = image
                let row = indexPath.row + 1
                if (row % 4 == 0 && indexPath.row != 0) {
                    cell.userAvatarImageView.image = cell.userAvatarImageView.image?.inverseImage()
                }
            }
          } catch {
            print(error)
          }
        }

        cell.onReuse = {
          if let token = token {
            self.loader.cancelLoad(token)
          }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let user: User
        
        if isFiltering {
            user = filteredUsers[indexPath.row]
        }
        else {
            user = users[indexPath.row]
        }
        
        if let storyboard = self.storyboard {
            let detailView = storyboard.instantiateViewController(withIdentifier: "DetailViewController") as! DetailViewController
            detailView.user = user
            
            DispatchQueue.main.async {
                self.navigationController?.pushViewController(detailView, animated: true)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if !isFiltering {
            if indexPath.section == tableView.numberOfSections - 1 &&
                indexPath.row == tableView.numberOfRows(inSection: indexPath.section) - 1 {
                fetchUsers()
            }
        }
    }
}

extension MainViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        let searchBar = searchController.searchBar
        filterContentForSearchText(searchBar.text!)
      }
    
    func filterContentForSearchText(_ searchText: String) {
          
        do {
            let request = User.fetchRequest() as NSFetchRequest<User>
            let loginPred = NSPredicate(format: "login CONTAINS %@", "\(searchText.lowercased())")
            let notesPred = NSPredicate(format: "notes CONTAINS %@", "\(searchText)")
            
            let predicates = NSCompoundPredicate(type: .or, subpredicates: [loginPred, notesPred])
            request.predicate = predicates
            
            filteredUsers = try self.context.fetch(request)
        }
        catch {
            //
        }
        
        tableView.reloadData()
    }
}
