//
//  UserTableViewCell.swift
//  Tawk_Exam
//
//  Created by Michael Joseph Redoble on 05/06/2021.
//

import UIKit

class UserTableViewCell: UITableViewCell {

    @IBOutlet weak var userAvatarImageView: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var userDetailLabel: UILabel!
    @IBOutlet weak var firstLetterLabel: UILabel!
    
    var onReuse: () -> Void = {}

    override func prepareForReuse() {
        super.prepareForReuse()
        onReuse()
        userAvatarImageView.image = nil
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.updateView()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func updateView() {
        userAvatarImageView.layer.borderWidth = 0.5
        userAvatarImageView.layer.masksToBounds = false
        userAvatarImageView.layer.borderColor = UIColor.black.cgColor
        userAvatarImageView.layer.cornerRadius = userAvatarImageView.frame.height/2
        userAvatarImageView.clipsToBounds = true
    }
}
