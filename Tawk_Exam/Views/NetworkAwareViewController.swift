//
//  UIViewController+Extension.swift
//  Tawk_Exam
//
//  Created by Michael Joseph Redoble on 07/06/2021.
//

import Foundation
import UIKit
import Network

class NetworkAwareViewController: UIViewController {
    
    var hasInternetConnection = false
    
    override func viewDidLoad() {
        monitorNetwork()
    }
    
    func hasInternetConnectivity() {
        //TODO: Show if has internet connectivity
    }
    
    func dontHaveInternetConnectivity() {
        //TODO: Show if has internet connectivity
    }
    
    func monitorNetwork() {
        let monitor = NWPathMonitor()
        monitor.pathUpdateHandler = { path in
            if path.status == .satisfied {
                self.hasInternetConnection = true
                self.hasInternetConnectivity()
            }
            else {
                self.hasInternetConnection = false
                self.dontHaveInternetConnectivity()
            }
        }
        
        let queue = DispatchQueue(label: "Network")
        monitor.start(queue: queue)
    }
}
